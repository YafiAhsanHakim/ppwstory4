from django.db import models
from django.forms import ModelForm

class ClassYear(models.Model):
    BATCH_CHOICES= [('2019 (Maung)', '2019 (Maung)'),
    ('2018 (Quanta)', '2018 (Quanta)'),
    ('2017 (Tarung)', '2017 (Tarung)'),
    ('2016 (Omega)', '2016 (Omega)'),
    ('Others', 'Others')]
    batch = models.CharField(max_length=255, choices=BATCH_CHOICES, default='NULL')

    def __str__(self):
        return self.batch


class Friend(models.Model):
	name = models.CharField(max_length=30)
	hobby = models.CharField(max_length=30)
	favoriteFood = models.CharField(max_length=30)
	favoriteDrink = models.CharField(max_length=30)
	year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)
	
	def __str__(self):
		return self.name


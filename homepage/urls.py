from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('profile/', views.aboutMe, name='aboutMe'),
	path('skills/', views.mySkills, name='mySkills'),
	path('projects/', views.myProjects, name='myProjects'),
	path('form/', views.form, name='form'),
	path('testform/', views.testform, name='testform'),
	path('myfriend/', views.myfriend, name='myfriend'),
]
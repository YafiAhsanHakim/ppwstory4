from django.shortcuts import render
from .models import Friend, ClassYear
from django.shortcuts import redirect
from . import forms

# Create your views here.
def home(request):
    return render(request, 'home bootstrap.html')
	
def aboutMe(request):
    return render(request, 'About me bootstrap.html')

def mySkills(request):
    return render(request, 'My Skills bootstrap.html')
	
def myProjects(request):
    return render(request, 'My Projects bootstrap.html')
	
def form(request):
	return render(request, 'form.html')
	
def testform(request):
    if request.method == 'POST':
        form = forms.FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:myfriend')
    else:
        form = forms.FriendForm()
    return render(request, 'testform.html', {'form': form})

def myfriend(request):
    allFriend = Friend.objects.all()
    year = ClassYear.objects.all()
    response = {'allFriend' : allFriend, 'year': year}
    return render(request, 'myfriend.html', response)
